/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jadeagents;

/**
 *
 * @author Marcin
 */
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import java.util.Random;

public class Soldier extends Agent {
 Agent tomek=this;
 Random r;
 int liczba=0;
    public Soldier(){
        r=new Random();
        liczba=r.nextInt(10)+1;
        liczba*=1000;
    }
    @Override
    protected void setup() {
        System.out.println("COS TU DZIALA "+this.getName());
         String nickname = "commander";
		AID id = new AID(nickname, AID.ISLOCALNAME);
        addBehaviour(new TickerBehaviour(tomek, 10000) {
            protected void onTick() {
                System.out.println("WYSYLAM WIADOMOSC");
                 ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                msg.addReceiver(new AID("commander", AID.ISLOCALNAME));
                msg.setContent("Melduję się");
               // send(msg);
              
               tomek.send(msg);;
            }
        });
    }

    @Override
    protected void takeDown() { //opcjonalnie
        // operacje wykonywane bezpośrednio przed usunięciem agenta
    }
}
