/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jadeagents;

import jade.core.Runtime;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.ProfileException;

import jade.util.leap.Properties;

/**
 *
 * @author Marcin
 */
public class JadeAgents {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // Create the Profile 
            ProfileImpl p = null;

            Properties pp = new Properties();
            pp.setProperty(Profile.GUI, "true");
            p = new ProfileImpl(pp);

            // Start a new JADE runtime system
            Runtime.instance().setCloseVM(true);
            //#PJAVA_EXCLUDE_BEGIN
            // Check whether this is the Main Container or a peripheral container
            if (p.getBooleanProperty(Profile.MAIN, true)) {
                Runtime.instance().createMainContainer(p);
            } else {
                Runtime.instance().createAgentContainer(p);
            }

        } catch (Exception pe) {
            System.err.println("Error creating the Profile [" + pe.getMessage() + "]");
            pe.printStackTrace();
            //System.err.println("Usage: java jade.Boot <filename>");
            System.exit(-1);
        } 
    }

}
