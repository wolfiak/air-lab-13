/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ontology;

import jade.content.onto.BeanOntology;
import jade.content.onto.Ontology;

/**
 *
 * @author Marcin
 */
public class SearchingOntology extends BeanOntology {

    public static final String NAME = "searching-ontology";
    private static final Ontology instance = new SearchingOntology();

    private SearchingOntology() {
        super(NAME);
        try {
            add(SearchRequest.class);
            add(SearchResponse.class);
            add(Find.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
