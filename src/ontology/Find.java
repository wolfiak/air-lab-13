
package ontology;

import jade.content.AgentAction;
import jade.content.Concept;

/**
 *
 * @author Marcin
 */
public class Find implements Concept, AgentAction {
    private SearchRequest request;

    public SearchRequest getRequest() {
        return request;
    }

    public void setRequest(SearchRequest request) {
        this.request = request;
    }
    
}
